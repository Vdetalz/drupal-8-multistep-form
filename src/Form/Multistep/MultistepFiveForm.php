<?php

namespace Drupal\multistep_form\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class MultistepFiveForm.
 *
 * @package Drupal\multistep_form\Form\Multistep
 */
class MultistepFiveForm extends MultistepFormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'multistep_form_five';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    // Confirm step1.
    $markup1 = $this->getConfirmDetailsOne();

    $form['preview1']                     = [
      '#markup' => $markup1,
    ];
    $form['preview1']['actions']['step1'] = array(
      '#type'       => 'link',
      '#title'      => $this->t('Edit'),
      '#attributes' => array(
        'class' => array('button'),
      ),
      '#suffix'     => '<hr />',
      '#weight'     => 0,
      '#url'        => Url::fromRoute('multistep_form.multistep_one'),
    );
    // Confirm Step2.
    $markup2 = $this->getConfirmDetailsTwo();

    $form['preview2']                     = [
      '#markup' => $markup2,
    ];
    $form['preview2']['actions']['step2'] = array(
      '#type'       => 'link',
      '#title'      => $this->t('Edit'),
      '#attributes' => array(
        'class' => array('button'),
      ),
      '#suffix'     => '<hr />',
      '#weight'     => 0,
      '#url'        => Url::fromRoute('multistep_form.multistep_two'),
    );
    // Confirm Step3.
    $markup3 = $this->getConfirmDetailsThree();

    $form['preview3']                     = [
      '#markup' => $markup3,
    ];
    $form['preview3']['actions']['step3'] = array(
      '#type'       => 'link',
      '#title'      => $this->t('Edit'),
      '#attributes' => array(
        'class' => array('button'),
      ),
      '#suffix'     => '<hr />',
      '#weight'     => 0,
      '#url'        => Url::fromRoute('multistep_form.multistep_three'),
    );
    // Confirm Step4.
    $markup4 = $this->getConfirmDetailsFour();

    $form['preview4']                     = [
      '#markup' => $markup4,
    ];
    $form['preview4']['actions']['step4'] = array(
      '#type'       => 'link',
      '#title'      => $this->t('Edit'),
      '#attributes' => array(
        'class' => array('button'),
      ),
      '#suffix'     => '<hr />',
      '#weight'     => 0,
      '#url'        => Url::fromRoute('multistep_form.multistep_four'),
    );

    $form['step5'] = array(
      '#type' => 'processed_text',
      '#text' => $this->t('Please confirm details.'),
    );

    $form['term']                   = array(
      '#type'   => 'table',
      '#header' => array(
        $this->t('Terms'),
      ),
    );
    $form['term'][1]['#attributes'] = array(
      'class' => array(
        'table',
      ),
    );
    $form['term'][1]['submission']  = array(
      '#type'          => 'checkbox',
      '#title'         => $this->t("I/we accept the'Submission Terms'"),
      '#default_value' => $this->store->get('submission') ? $this->store->get('submission') : '',
    );

    $form['actions']['previous'] = array(
      '#type'       => 'link',
      '#title'      => $this->t('Previous'),
      '#attributes' => array(
        'class' => array('button'),
      ),
      '#weight'     => 0,
      '#url'        => Url::fromRoute('multistep_form.multistep_four'),
    );

    $form['actions']['submit']['#value'] = $this->t('Next');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('submission', $form_state->getValue('submission'));

    // Delete data.
    parent::deleteStore($this->getKeys());

    $form_state->setRedirect('multistep_form.multistep_six');
  }

  /**
   * Confirm details.
   * @return string
   */
  public function getConfirmDetailsOne() {

    $confirm[0]['key']   = $this->t('Winery name');
    $confirm[0]['value'] = $this->store->get('name') ? $this->store->get('name') : '';
    $confirm[1]['key']   = $this->t('Company Trading Name');
    $confirm[1]['value'] = $this->store->get('second_name') ? $this->store->get('second_name') : '';
    $confirm[2]['key']   = $this->t('Are You NZ Winery?');
    $confirm[2]['value'] = $this->store->get('tick1') ? t('Yes') : t('No');
    $confirm[3]['key']   = $this->t('Street Address');

    $confirm[3]['value'] = $this->store->get('street_address1') ? $this->store->get('street_address1') : '';
    $confirm[3]['value'] .= $this->store->get('city1') ? ', ' . $this->store->get('city1') : '';
    $confirm[3]['value'] .= $this->store->get('post_code1') ? ', ' . $this->store->get('post_code1') : '';
    $confirm[3]['value'] .= $this->store->get('postal_address1') ? ', ' . $this->store->get('postal_address1') : '';

    $confirm[4]['key']   = $this->t('Phone');
    $confirm[4]['value'] = $this->store->get('phone_s1') ? $this->store->get('phone_s1') : '';
    $confirm[5]['key']   = $this->t('Site');
    $confirm[5]['value'] = $this->store->get('site') ? $this->store->get('site') : '';

    $head   = $this->t('Winery Details');
    $markup = $this->getMarkup($confirm, $head);

    return $markup;
  }

  /**
   * Confirm details.
   * @return string
   */
  public function getConfirmDetailsTwo() {
    $num     = $this->store->get('num_names2');
    $confirm = array();

    for ($i = 0; $i < $num; $i++) {
      $confirm[$i][0]['key']   = $this->t('Name');
      $confirm[$i][0]['value'] = $this->store->get('first_name' . $i) ? $this->store->get('first_name' . $i) : '';;
      $confirm[$i][0]['value'] .= $this->store->get('last_name' . $i) ? ' ' . $this->store->get('last_name' . $i) : '';;
      $confirm[$i][1]['key']   = $this->t('Primary Contact');
      $confirm[$i][1]['value'] = ($this->store->get('first_name' . $i) == 1) ? 'Yes' : 'No';;
      $confirm[$i][2]['key']   = $this->t('Role');
      $confirm[$i][2]['value'] = $this->store->get('role' . $i) ? $this->store->get('role' . $i) : '';;
      $confirm[$i][3]['key']   = $this->t('Email');
      $confirm[$i][3]['value'] = $this->store->get('user_email' . $i) ? $this->store->get('user_email' . $i) : '';;
      $confirm[$i][4]['key']   = $this->t('Phone');
      $confirm[$i][4]['value'] = $this->store->get('phone_s2' . $i) ? $this->store->get('phone_s2' . $i) : '';;
      $confirm[$i][5]['key']   = $this->t('Mobile');
      $confirm[$i][5]['value'] = $this->store->get('mobile' . $i) ? $this->store->get('mobile' . $i) : '';;

    }

    $head   = $this->t('Winery Contacts');
    $markup = $this->getAddMoreMarkup($confirm, $head);

    return $markup;

  }

  /**
   * Confirm details.
   * @return string
   */
  public function getConfirmDetailsThree() {
    $num     = $this->store->get('num_names3');
    $confirm = array();

    for ($i = 0; $i < $num; $i++) {
      $confirm[$i][0]['key']   = $this->t('Varietal');
      $confirm[$i][0]['value'] = $this->store->get('varietal' . $i) ? $this->store->get('varietal' . $i) : '';;
      $confirm[$i][1]['key']   = $this->t('Grape Variety');
      $confirm[$i][1]['value'] = $this->store->get('grape_variety') ? $this->store->get('grape_variety' . $i) : '';;
      $confirm[$i][2]['key']   = $this->t('Label');
      $confirm[$i][2]['value'] = $this->store->get('label' . $i) ? $this->store->get('label' . $i) : '';;
      $confirm[$i][3]['key']   = $this->t('Lot Number');
      $confirm[$i][3]['value'] = $this->store->get('lot_number' . $i) ? $this->store->get('lot_number' . $i) : '';;
      $confirm[$i][4]['key']   = $this->t('Sustainable Wine Code');
      $confirm[$i][4]['value'] = $this->store->get('wine_code' . $i) ? $this->store->get('wine_code' . $i) : '';;
      $confirm[$i][5]['key']   = $this->t('Grape Region');
      $confirm[$i][5]['value'] = $this->store->get('grape_region' . $i) ? $this->store->get('grape_region' . $i) : '';;
      $confirm[$i][6]['key']   = $this->t('Winery Location');
      $confirm[$i][6]['value'] = $this->store->get('winery_location' . $i) ? $this->store->get('winery_location' . $i) : '';;
      $confirm[$i][7]['key']   = $this->t('Bottles per Case');
      $confirm[$i][7]['value'] = $this->store->get('case' . $i) ? $this->store->get('case' . $i) : '';;
      $confirm[$i][8]['key']   = $this->t('Bottle Size');
      $confirm[$i][8]['value'] = $this->store->get('size' . $i) ? $this->store->get('size' . $i) : '';;
      $confirm[$i][9]['key']   = $this->t('Case Price (excl excise)');
      $confirm[$i][9]['value'] = $this->store->get('excl' . $i) ? $this->store->get('excl' . $i) : '';;
      $confirm[$i][10]['key']   = $this->t('Case Price (incl excise)');
      $confirm[$i][10]['value'] = $this->store->get('incl' . $i) ? $this->store->get('incl' . $i) : '';;
      $confirm[$i][11]['key']   = $this->t('Minimum Cases Required for Chardonnay is 250 cases (12 x 750ml) )');
      $confirm[$i][11]['value'] = $this->store->get('minimum' . $i) ? $this->store->get('minimum' . $i) : '';;
      $confirm[$i][12]['key']   = $this->t('Supply Volume Maximum Cases Available (12 x 750ml)');
      $confirm[$i][12]['value'] = $this->store->get('maximum' . $i) ? $this->store->get('maximum' . $i) : '';;

    }

    $head   = $this->t('Wine Details');
    $markup = $this->getAddMoreMarkup($confirm, $head);

    return $markup;

  }

  /**
   * Confirm details.
   * @return string
   */
  public function getConfirmDetailsFour() {
    $confirm[0]['key']   = $this->t('Comments');
    $confirm[0]['value'] = $this->store->get('comment') ? $this->store->get('comment') : '';

    $head   = $this->t('Comments');
    $markup = $this->getMarkup($confirm, $head);

    return $markup;

  }

  /**
   * Murkup for the confirm form.
   *
   * @param $array
   *   Form_state values.
   * @param $head
   *   Head for step confirm information.
   *
   * @return string
   */
  public function getMarkup($array, $head) {

    $markup = '<div><h2>' . $head . '</h2>';

    for ($i = 0; $i < count($array); $i++) {
      $markup .= '<p><strong>' . $array[$i]['key'] . ':</strong> ' . $array[$i]['value'] . '</p>';
    }
    $markup .= '</div>';

    return $markup;
  }

  /**
   * Murkup for the confirm form with add more button.
   *
   * @param $array
   * @param $head
   *
   * @return string
   */
  public function getAddMoreMarkup($array, $head) {

    $markup = '<div><h2>' . $head . '</h2>';

    for ($i = 0; $i < count($array); $i++) {
      $markup .= '<h3>' . ($i + 1) . ')</h3>';
      for ($k = 0; $k < count($array[$i]); $k++) {
        $markup .= '<p><strong>' . $array[$i][$k]['key'] . ':</strong> ' . $array[$i][$k]['value'] . '</p>';
      }
    }
    $markup .= '</div>';

    return $markup;

  }

  /**
   * Contain array of the form's keys.
   *
   * @return array
   */
  public function getKeys() {
    $keys = array(
      'name',
      'site',
      'tick1',
      'phone_s1',
      'street_address1',
      'street_address2',
      'city1',
      'city2',
      'post_code1',
      'post_code2',
      'postal_address',
      'postal_address1',
      'postal_address2',
      'second_name',
      'contact',
      'first_name',
      'last_name',
      'role',
      'user_email',
      'phone_s2',
      'mobile',
      'varietal',
      'grape_variety',
      'label',
      'lot_number',
      'vintage',
      'wine_code',
      'grape_region',
      'winery_location',
      'case',
      'size',
      'excl',
      'incl',
      'minimum',
      'maximum',
      'comment',
      'num_names2',
      'num_names3',
    );
    return $keys;
  }

}
