<?php

namespace Drupal\multistep_form\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Class MultistepThreeForm.
 *
 * @package Drupal\multistep_form\Form\Multistep
 */
class MultistepThreeForm extends MultistepFormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'multistep_form_three';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['multiple_destinations']          = array(
      '#type'   => 'fieldset',
      '#title'  => 'Multiple Destinations',
      '#tree'   => TRUE,
      '#prefix' => '<div id="multi-replace">',
      '#suffix' => '</div>',
    );
    $form['multiple_destinations']['step3'] = array(
      '#type' => 'processed_text',
      '#text' => $this->t('It can be installed as regular Drupal module, and provides an admin interface to let you pick the hooks you want.'),
    );

    $num = $this->store->get('num_names3');

    if (empty($num)) {
      $this->store->set('num_names3', 1);
      $num = $this->store->get('num_names3');
    }

    for ($i = 0; $i < $num; $i++) {
      // Table 1.
      $form['multiple_destinations'][$i]['wine_details1']                     = array(
        '#type'    => 'table',
        '#caption' => $this->t('Wine Submission Number : [XXXX-@i]', array('@i' => $i + 1)),
        '#header'  => array(
          $this->t('Varietal'),
          $this->t('Grape Variety'),
          $this->t('Label'),
          $this->t('Lot Number'),
        ),
      );
      $form['multiple_destinations'][$i]['wine_details1'][1]['#attributes']   = array(
        'class' => array(
          'table',
        ),
      );
      $form['multiple_destinations'][$i]['wine_details1'][1]['varietal']      = array(
        '#type'          => 'select',
        '#title'         => t('Varietal'),
        '#title_display' => 'invisible',
        '#options'       => array(
          'No' => t('No'),
          'Yes' => t('Yes'),
        ),
        '#default_value' => $this->store->get('varietal' . $i) ? $this->store->get('varietal' . $i) : '',
      );
      $form['multiple_destinations'][$i]['wine_details1'][1]['grape_variety'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Grape Variety'),
        '#title_display' => 'invisible',
        '#default_value' => $this->store->get('grape_variety' . $i) ? $this->store->get('grape_variety' . $i) : '',
        '#size'          => 30,
      );
      $form['multiple_destinations'][$i]['wine_details1'][1]['label']         = array(
        '#type'          => 'textfield',
        '#title'         => t('Label'),
        '#title_display' => 'invisible',
        '#default_value' => $this->store->get('label' . $i) ? $this->store->get('label' . $i) : '',
        '#size'          => 30,
      );
      $form['multiple_destinations'][$i]['wine_details1'][1]['lot_number']    = array(
        '#type'          => 'textfield',
        '#title'         => t('Lot Number'),
        '#title_display' => 'invisible',
        '#default_value' => $this->store->get('lot_number' . $i) ? $this->store->get('lot_number' . $i) : '',
        '#size'          => 30,
      );

      // Table 2.
      $form['multiple_destinations'][$i]['wine_details2']                       = array(
        '#type'   => 'table',
        '#header' => array(
          $this->t('Vintage'),
          $this->t('Sustainable Wine Code'),
          $this->t('Grape Region'),
          $this->t('Winery Location'),
        ),
      );
      $form['multiple_destinations'][$i]['wine_details2'][1]['#attributes']     = array(
        'class' => array(
          'table',
        ),
      );
      $form['multiple_destinations'][$i]['wine_details2'][1]['vintage']         = array(
        '#type'          => 'select',
        '#title_display' => 'invisible',
        '#title'         => t('Vintage'),
        '#options'       => array(
          'No' => t('No'),
          'Yes' => t('Yes'),
        ),
        '#default_value' => $this->store->get('vintage') ? $this->store->get('vintage') : '',
      );
      $form['multiple_destinations'][$i]['wine_details2'][1]['wine_code']       = array(
        '#type'          => 'select',
        '#title'         => t('Sustainable Wine Code'),
        '#title_display' => 'invisible',
        '#options'       => array(
          'No' => t('No'),
          'Yes' => t('Yes'),
        ),
        '#default_value' => $this->store->get('wine_code' . $i) ? $this->store->get('wine_code' . $i) : '',
      );
      $form['multiple_destinations'][$i]['wine_details2'][1]['grape_region']    = array(
        '#type'          => 'select',
        '#title'         => t('Grape Region'),
        '#title_display' => 'invisible',
        '#options'       => array(
          'No' => t('No'),
          'Yes' => t('Yes'),
        ),
        '#default_value' => $this->store->get('grape_region' . $i) ? $this->store->get('grape_region' . $i) : '',
      );
      $form['multiple_destinations'][$i]['wine_details2'][1]['winery_location'] = array(
        '#type'          => 'select',
        '#title'         => t('Winery Location'),
        '#title_display' => 'invisible',
        '#options'       => array(
          'No' => t('No'),
          'Yes' => t('Yes'),
        ),
        '#default_value' => $this->store->get('winery_location' . $i) ? $this->store->get('winery_location' . $i) : '',
      );

      // Table 3.
      $form['multiple_destinations'][$i]['wine_details3']                   = array(
        '#type' => 'table',
      );
      $form['multiple_destinations'][$i]['wine_details3'][1]['#attributes'] = array(
        'class' => array(
          'table',
        ),
      );
      $form['multiple_destinations'][$i]['wine_details3'][1]['case']        = array(
        '#type'          => 'select',
        '#title'         => t('Bottles per Case'),
        '#options'       => array(
          'No' => t('No'),
          'Yes' => t('Yes'),
        ),
        '#default_value' => $this->store->get('case' . $i) ? $this->store->get('case' . $i) : '',
      );
      $form['multiple_destinations'][$i]['wine_details3'][1]['size']        = array(
        '#type'          => 'select',
        '#title'         => t('Bottle Size'),
        '#options'       => array(
          'No' => t('No'),
          'Yes' => t('Yes'),
        ),
        '#default_value' => $this->store->get('size' . $i) ? $this->store->get('size' . $i) : '',
      );

      $form['multiple_destinations'][$i]['wine_details3'][1]['excl']                = array(
        '#type'          => 'textfield',
        '#title'         => t('Case Price(excl excise)'),
        '#default_value' => $this->store->get('excl' . $i) ? $this->store->get('excl' . $i) : '',
        '#size'          => 10,
      );
      $form['multiple_destinations'][$i]['wine_details3'][1]['incl']                = array(
        '#type'          => 'textfield',
        '#title'         => t('Case Price(incl excise)'),
        '#default_value' => $this->store->get('incl' . $i) ? $this->store->get('incl' . $i) : '',
        '#size'          => 10,
      );
      $form['multiple_destinations'][$i]['wine_details3'][1]['colspan1']            = array(
        '#plain_text'         => 'Minimum Cases Required for [Varietal] is [XXX] cases ([bottles] x [size]ml)',
        '#wrapper_attributes' => array(
          'colspan' => 2,
          'class'   => array('colspan'),
        ),
      );
      $form['multiple_destinations'][$i]['wine_details3'][1]['colspan1']['minimum'] = array(
        '#type'          => 'radios',
        '#title'         => t('Can you meet this minimum requirement?'),
        '#options'       => array(
          'Yes' => $this->t('Yes'),
          'No' => $this->t('No'),
        ),
        '#title_display' => 'before',
        '#default_value' => $this->store->get('minimum' . $i) ? $this->store->get('minimum' . $i) : '',
      );
      $form['multiple_destinations'][$i]['wine_details3'][1]['colspan2']            = array(
        '#plain_text'         => 'Supply Volume Maximum Cases Available ([bottles] x [size]ml)',
        '#wrapper_attributes' => array(
          'colspan' => 2,
          'class'   => array('colspan'),
        ),
      );
      $form['multiple_destinations'][$i]['wine_details3'][1]['colspan2']['maximum'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Cases'),
        '#default_value' => $this->store->get('maximum' . $i) ? $this->store->get('maximum' . $i) : '',
        '#size'          => 10,
      );
    }
    $form_state->setCached(FALSE);

    $form['actions']['previous'] = array(
      '#type'       => 'link',
      '#title'      => $this->t('Previous'),
      '#attributes' => array(
        'class' => array('button'),
      ),
      '#weight'     => 0,
      '#url'        => Url::fromRoute('multistep_form.multistep_two'),
    );

    if ($num > 1) {
      $form['actions']['remove_name'] = [
        '#type'                    => 'submit',
        '#value'                   => t('Remove'),
        '#submit'                  => array('::removeCallback'),
        '#limit_validation_errors' => array(),
        '#ajax'                    => [
          'callback' => '::addMoreCallback',
          'wrapper'  => 'multi-replace',
        ],
      ];
    }

    $form['actions']['add'] = array(
      '#type'   => 'submit',
      '#value'  => $this->t('Add More'),
      '#submit' => array('::addMoreAddOne'),
      '#weight' => 0,
      '#ajax'   => array(
        'callback' => '::addMoreCallback',
        'wrapper'  => 'multi-replace',
      ),
    );

    $form['actions']['submit']['#value'] = $this->t('Next');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values   = $form_state->getValues();
    $values_m = $values['multiple_destinations'];

    for ($i = 0; $i < count($values_m); $i++) {

      $this->store->set('varietal' . $i, $values_m[$i]['wine_details1'][1]['varietal']);
      $this->store->set('grape_variety' . $i, $values_m[$i]['wine_details1'][1]['grape_variety']);
      $this->store->set('label' . $i, $values_m[$i]['wine_details1'][1]['label']);
      $this->store->set('lot_number' . $i, $values_m[$i]['wine_details1'][1]['lot_number']);
      $this->store->set('vintage' . $i, $values_m[$i]['wine_details2'][1]['vintage']);
      $this->store->set('wine_code' . $i, $values_m[$i]['wine_details2'][1]['wine_code']);
      $this->store->set('grape_region' . $i, $values_m[$i]['wine_details2'][1]['grape_region']);
      $this->store->set('winery_location' . $i, $values_m[$i]['wine_details2'][1]['winery_location']);
      $this->store->set('case' . $i, $values_m[$i]['wine_details3'][1]['case']);
      $this->store->set('size' . $i, $values_m[$i]['wine_details3'][1]['size']);
      $this->store->set('excl' . $i, $values_m[$i]['wine_details3'][1]['excl']);
      $this->store->set('incl' . $i, $values_m[$i]['wine_details3'][1]['incl']);
      $this->store->set('minimum' . $i, $values_m[$i]['wine_details3'][1]['colspan1']['minimum']);
      $this->store->set('maximum' . $i, $values_m[$i]['wine_details3'][1]['colspan2']['maximum']);
    }

    // Save the data.
    parent::saveData();
    $form_state->setRedirect('multistep_form.multistep_four');
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the fields in it.
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#multi-replace', $form['multiple_destinations']));
    return $response;
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addMoreAddOne(array &$form, FormStateInterface $form_state) {
    $val = $this->store->get('num_names3');
    $val++;
    $this->store->set('num_names3', $val);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $this->store->get('num_names3');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $this->store->set('num_names3', $remove_button);
    }
    $form_state->setRebuild();
  }

}
