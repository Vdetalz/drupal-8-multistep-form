<?php

namespace Drupal\multistep_form\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class MultistepOneForm.
 *
 * @package Drupal\multistep_form\Form\Multistep
 */
class MultistepOneForm extends MultistepFormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'multistep_form_one';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    //$form['mytable']['#header'][0] = 'a';
    $form['mytable'][] = array('#attributes' => array('class' => array('foo')));

    $form['step1']           = array(
      '#type' => 'processed_text',
      '#text' => $this->t('Enter details for your winery below.'),
    );
    $form['name']            = array(
      '#type'          => 'textfield',
      '#title'         => $this->t('Winery name'),
      '#default_value' => $this->store->get('name') ? $this->store->get('name') : '',
    );
    $form['tick1']           = array(
      '#type'          => 'checkbox',
      '#title'         => $this->t('Company Trading Name'),
      '#title_display' => 'before',
      '#description'   => $this->t('Tick if same as Winery Name'),
      '#default_value' => $this->store->get('tick1') ? $this->store->get('tick1') : '',
    );
    $form['second_name']     = array(
      '#type'          => 'textfield',
      '#default_value' => $this->store->get('second_name') ? $this->store->get('second_name') : '',
      '#states'        => array(
        'disabled' => array(
          ':input[name="tick1"]' => array('checked' => FALSE),
        ),
        'enabled'  => array(
          ':input[name="tick1"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['street_address1'] = array(
      '#type'          => 'textfield',
      '#title'         => $this->t('Street Address'),
      '#default_value' => $this->store->get('street_address1') ? $this->store->get('street_address1') : '',
    );
    $form['street_address2'] = array(
      '#type'          => 'textfield',
      '#default_value' => $this->store->get('street_address2') ? $this->store->get('street_address2') : '',
    );
    $form['city1']           = array(
      '#type'          => 'textfield',
      '#title'         => $this->t('City'),
      '#default_value' => $this->store->get('city1') ? $this->store->get('city1') : '',
    );
    $form['post_code1']      = array(
      '#type'          => 'textfield',
      '#title'         => $this->t('Post Code'),
      '#default_value' => $this->store->get('post_code1') ? $this->store->get('post_code1') : '',
      '#size'          => 25,
    );

    $form['link1']           = array(
      '#type'  => 'link',
      '#title' => $this->t('NZ Post PostCode Finder'),
      '#url'   => Url::fromUri('https://www.nzpost.co.nz/tools/address-postcode-finder'),
    );
    $form['postal_address']  = array(
      '#type'          => 'checkbox',
      '#title'         => $this->t('Postal Address'),
      '#title_display' => 'before',
      '#description'   => $this->t('Tick if Postal Address is the same as Street Address'),
      '#default_value' => $this->store->get('postal_address') ? $this->store->get('postal_address') : '',
    );
    $form['postal_address1'] = array(
      '#type'          => 'textfield',
      '#default_value' => $this->store->get('postal_address1') ? $this->store->get('postal_address1') : '',
      '#states'        => array(
        'disabled' => array(
          ':input[name="postal_address"]' => array('checked' => FALSE),
        ),
        'enabled'  => array(
          ':input[name="postal_address"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['postal_address2'] = array(
      '#type'          => 'textfield',
      '#default_value' => $this->store->get('postal_address2') ? $this->store->get('postal_address2') : '',
      '#states'        => array(
        'disabled' => array(
          ':input[name="postal_address"]' => array('checked' => FALSE),
        ),
        'enabled'  => array(
          ':input[name="postal_address"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['city2']           = array(
      '#type'          => 'textfield',
      '#title'         => $this->t('City'),
      '#default_value' => $this->store->get('city2') ? $this->store->get('city2') : '',
    );
    $form['post_code2']      = array(
      '#type'          => 'textfield',
      '#title'         => $this->t('Post Code'),
      '#default_value' => $this->store->get('post_code2') ? $this->store->get('post_code2') : '',
      '#size'          => 25,
    );

    $form['link2']    = array(
      '#type'  => 'link',
      '#title' => $this->t('NZ Post PostCode Finder'),
      '#url'   => Url::fromUri('https://www.nzpost.co.nz/tools/address-postcode-finder'),
    );
    $form['phone_s1'] = array(
      '#type'          => 'textfield',
      '#title'         => $this->t('Phone'),
      '#default_value' => $this->store->get('phone_s1') ? $this->store->get('phone_s1') : '',
    );

    $form['site'] = array(
      '#type'          => 'textfield',
      '#title'         => $this->t('Website'),
      '#default_value' => $this->store->get('site') ? $this->store->get('site') : '',
    );


    $form['actions']['submit']['#value'] = $this->t('Next');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('name', $form_state->getValue('name'));
    $this->store->set('tick1', $form_state->getValue('tick1'));
    $this->store->set('second_name', $form_state->getValue('second_name'));
    $this->store->set('street_address1', $form_state->getValue('street_address1'));
    $this->store->set('street_address2', $form_state->getValue('street_address2'));
    $this->store->set('city1', $form_state->getValue('city1'));
    $this->store->set('post_code1', $form_state->getValue('post_code1'));
    $this->store->set('postal_address', $form_state->getValue('postal_address'));
    $this->store->set('postal_address1', $form_state->getValue('postal_address1'));
    $this->store->set('postal_address2', $form_state->getValue('postal_address2'));
    $this->store->set('city2', $form_state->getValue('city2'));
    $this->store->set('post_code2', $form_state->getValue('post_code2'));
    $this->store->set('phone_s1', $form_state->getValue('phone_s1'));
    $this->store->set('site', $form_state->getValue('site'));

    // Save the data.
    parent::saveData();

    $form_state->setRedirect('multistep_form.multistep_two');
  }

}
