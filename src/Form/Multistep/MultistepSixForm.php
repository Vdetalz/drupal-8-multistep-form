<?php

namespace Drupal\multistep_form\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class MultistepSixForm.
 *
 * @package Drupal\multistep_form\Form\Multistep
 */
class MultistepSixForm extends MultistepFormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'multistep_form_six';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['finished'] = array(
      '#type' => 'processed_text',
      '#text' => $this->t('It can be installed as regular Drupal module, and provides an admin interface to let you pick the hooks you want.'),
    );

    $form['actions']['submit']['#value'] = $this->t('Retur to Homepage');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('<front>');
  }

}
