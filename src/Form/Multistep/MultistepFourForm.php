<?php

namespace Drupal\multistep_form\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class MultistepFourForm.
 *
 * @package Drupal\multistep_form\Form\Multistep
 */
class MultistepFourForm extends MultistepFormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'multistep_form_four';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['step4'] = array(
      '#type' => 'processed_text',
      '#text' => $this->t('It you have a comments enter below.'),
    );

    $form['comment'] = array(
      '#type'          => 'textarea',
      '#title'         => $this->t('Comment'),
      '#title_display' => 'invisible',
      '#default_value' => $this->store->get('comment') ? $this->store->get('comment') : '',
    );

    $form['actions']['previous'] = array(
      '#type'       => 'link',
      '#title'      => $this->t('Previous'),
      '#attributes' => array(
        'class' => array('button'),
      ),
      '#weight'     => 0,
      '#url'        => Url::fromRoute('multistep_form.multistep_three'),
    );

    $form['actions']['submit']['#value'] = $this->t('Next');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('comment', $form_state->getValue('comment'));

    // Save the data.
    parent::saveData();
    $form_state->setRedirect('multistep_form.multistep_five');
  }

}
