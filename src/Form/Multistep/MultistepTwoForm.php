<?php

namespace Drupal\multistep_form\Form\Multistep;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Class MultistepTwoForm.
 *
 * @package Drupal\multistep_form\Form\Multistep
 */
class MultistepTwoForm extends MultistepFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multistep_form_two';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['multiple_destinations']          = array(
      '#type'   => 'fieldset',
      '#title'  => 'Multiple Destinations',
      '#tree'   => TRUE,
      '#prefix' => '<div id="multi-replace">',
      '#suffix' => '</div>',
    );
    $form['multiple_destinations']['step2'] = array(
      '#type' => 'processed_text',
      '#text' => $this->t('It can be installed as regular Drupal module, and provides an admin interface to let you pick the hooks you want.'),
    );

    $num = $this->store->get('num_names2');

    if (empty($num)) {
      $this->store->set('num_names2', 1);
      $num = $this->store->get('num_names2');
    }

    for ($i = 0; $i < $num; $i++) {
      $form['multiple_destinations'][$i]['contact']       = array(
        '#type'          => 'checkboxes',
        '#options'       => array(
          '1' => $this->t('Primary Contact'),
          '2' => $this->t('Send Correspondence'),
        ),
        '#title'         => $this->t('Contact'),
        '#title_display' => 'before',
        '#default_value' => $this->store->get('contact' . $i) ? $this->store->get('contact' . $i) : '',
      );
      $form['multiple_destinations'][$i]['first_name']    = array(
        '#type'          => 'textfield',
        '#title'         => $this->t('First Name'),
        '#default_value' => $this->store->get('first_name' . $i) ? $this->store->get('first_name' . $i) : '',
      );
      $form['multiple_destinations'][$i]['last_name']     = array(
        '#type'          => 'textfield',
        '#title'         => $this->t('Last Name'),
        '#default_value' => $this->store->get('last_name' . $i) ? $this->store->get('last_name' . $i) : '',
      );
      $form['multiple_destinations'][$i]['role']          = array(
        '#type'          => 'textfield',
        '#title'         => $this->t('Role'),
        '#default_value' => $this->store->get('role' . $i) ? $this->store->get('role' . $i) : '',
      );
      $form['multiple_destinations'][$i]['user_email']    = array(
        '#type'          => 'email',
        '#title'         => $this->t('Email Address'),
        '#default_value' => $this->store->get('user_email' . $i) ? $this->store->get('user_email' . $i) : '',
      );
      $form['multiple_destinations'][$i]['email_reenter'] = array(
        '#type'     => 'email',
        '#title'    => $this->t('Re-Enter Email Address'),
        '#required' => TRUE,
        '#ajax'     => array(
          'callback' => '::validateEmailAjax',
          'event'    => 'change',
          'progress' => array(
            'type'    => 'throbber',
            'message' => t('Verifying email...'),
          ),
        ),
        '#suffix'   => '<div class="email-validate"></div>',
      );
      $form['multiple_destinations'][$i]['phone_s2']      = array(
        '#title'         => $this->t('Phone'),
        '#default_value' => $this->store->get('phone_s2' . $i) ? $this->store->get('phone_s2' . $i) : '',
        '#type'          => 'tel',
        '#pattern'       => '^\d{8,14}',
        '#ajax'          => [
          'callback' => '::validatePhoneAjax',
          'event'    => 'change',
          'progress' => [
            'type'    => 'throbber',
            'message' => t('Verifying phone...'),
          ],
        ],
        '#suffix'        => '<div class="phone-validate"></div>',
      );
      $form['multiple_destinations'][$i]['mobile']        = array(
        '#title'         => $this->t('Mobile'),
        '#default_value' => $this->store->get('mobile' . $i) ? $this->store->get('mobile' . $i) : '',
        '#type'          => 'textfield',
      );
    }

    $form_state->setCached(FALSE);
    $form['actions']['previous'] = array(
      '#type'       => 'link',
      '#title'      => $this->t('Previous'),
      '#attributes' => array(
        'class' => array('button'),
      ),
      '#weight'     => 0,
      '#url'        => Url::fromRoute('multistep_form.multistep_one'),
    );

    if ($i > 0) {
      $form['actions']['remove_name'] = [
        '#type'                    => 'submit',
        '#value'                   => t('Remove'),
        '#submit'                  => array('::removeCallback'),
        '#limit_validation_errors' => array(),
        '#ajax'                    => [
          'callback' => '::addMoreCallback',
          'wrapper'  => 'multi-replace',
        ],
      ];
    }

    $form['actions']['add'] = array(
      '#type'   => 'submit',
      '#value'  => $this->t('Add More'),
      '#submit' => array('::addMoreAddOne'),
      '#weight' => 0,
      '#ajax'   => array(
        'callback' => '::addMoreCallback',
        'wrapper'  => 'multi-replace',
      ),
    );

    $form['actions']['submit']['#value'] = $this->t('Next');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values   = $form_state->getValues();
    $values_m = $values['multiple_destinations'];

    for ($i = 0; $i < count($values_m); $i++) {

      $email          = trim($values_m[$i]['user_email']);
      $re_enter_email = trim($values_m[$i]['email_reenter']);

      if (trim($re_enter_email) == NULL || strcmp($email, $re_enter_email) !== 0) {
        $form_state->setErrorByName('email_reenter', $this->t('Confirm your email.'));
      }
      $this->store->set('contact' . $i, $values_m[$i]['contact']);
      $this->store->set('first_name' . $i, $values_m[$i]['first_name']);
      $this->store->set('last_name' . $i, $values_m[$i]['last_name']);
      $this->store->set('role' . $i, $values_m[$i]['role']);
      $this->store->set('user_email' . $i, $values_m[$i]['user_email']);
      $this->store->set('phone_s2' . $i, $values_m[$i]['phone_s2']);
      $this->store->set('mobile' . $i, $values_m[$i]['mobile']);
    }
    // Save the data.
    parent::saveData();
    $form_state->setRedirect('multistep_form.multistep_three');
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the fields in it.
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#multi-replace', $form['multiple_destinations']));
    return $response;
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addMoreAddOne(array &$form, FormStateInterface $form_state) {
    $val = $this->store->get('num_names2');
    $val++;
    $this->store->set('num_names2', $val);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $this->store->get('num_names2');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $this->store->set('num_names2', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function validateEmailAjax(array &$form, FormStateInterface $form_state) {
    $response       = new AjaxResponse();
    $email          = trim($form_state->getValue('email'));
    $re_enter_email = trim($form_state->getValue('email_reenter'));

    if (strcmp($email, $re_enter_email) !== 0) {
      $response->addCommand(new HtmlCommand('.email-validate', t('Email is not valid.')));
    }
    else {
      $response->addCommand(new HtmlCommand('.email-validate', ''));
    }
    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function validatePhoneAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $phone    = $form_state->getValue('phone');
    $pattern  = '/^\d{8,14}/i';

    if (!preg_match($pattern, $phone)) {
      $response->addCommand(new HtmlCommand('.phone-validate', t('Telephone is not correct.')));
    }
    else {
      $response->addCommand(new HtmlCommand('.phone-validate', ''));
    }
    return $response;
  }

}
