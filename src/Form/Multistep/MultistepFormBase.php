<?php

namespace Drupal\multistep_form\Form\Multistep;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Class MultistepFormBase.
 *
 * @package Drupal\multistep_form\Form\Multistep
 */
abstract class MultistepFormBase extends FormBase {

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  private $sessionManager;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $currentUser;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * Constructs a \Drupal\multistep_form\Form\Multistep\MultistepFormBase.
   *
   * @param \Drupal\user\PrivateTempStoreFactory $temp_store_factory
   * @param \Drupal\Core\Session\SessionManagerInterface $session_manager
   * @param \Drupal\Core\Session\AccountInterface $current_user
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager, AccountInterface $current_user) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->sessionManager   = $session_manager;
    $this->currentUser      = $current_user;

    $this->store = $this->tempStoreFactory->get('multistep_data');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('session_manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Start a manual session for anonymous users.
    if ($this->currentUser->isAnonymous() && !isset($_SESSION['multistep_form_holds_session'])) {
      $_SESSION['multistep_form_holds_session'] = TRUE;
      $this->sessionManager->start();
    }

    $steps = $this->getSteps();

    $form            = array();
    $form['mytable'] = array(
      '#type'   => 'table',
      '#header' => $steps,
    );

    $form['actions']['#type']  = 'actions';
    $form['actions']['submit'] = array(
      '#type'        => 'submit',
      '#value'       => $this->t('Submit'),
      '#button_type' => 'primary',
      '#weight'      => 10,
    );

    return $form;
  }

  /**
   * Saves the data from the multistep form.
   */
  protected function saveData() {
    // Logic for saving data goes here...
    drupal_set_message($this->t('The form has been saved.'));
  }

  /**
   * Helper method that removes all the keys from the store collection used for
   * the multistep form.
   */
  protected function deleteStore($keys) {
    foreach ($keys as $key) {
      $this->store->delete($key);
    }

    drupal_set_message($this->t('The form has been deleted.'));
  }

  /**
   * Build steps and links of the header.
   */
  protected function getSteps() {
    $steps = array(
      Link::fromTextAndUrl('Winery Details', Url::fromUri('internal:' . '/multistep_form/multistep-one'))
          ->toString(),
      Link::fromTextAndUrl('Winery Contacts', Url::fromUri('internal:' . '/multistep_form/multistep-two'))
          ->toString(),
      Link::fromTextAndUrl('Wine Details', Url::fromUri('internal:' . '/multistep_form/multistep-three'))
          ->toString(),
      Link::fromTextAndUrl('Comments', Url::fromUri('internal:' . '/multistep_form/multistep-four'))
          ->toString(),
      Link::fromTextAndUrl('Confirm Details', Url::fromUri('internal:' . '/multistep_form/multistep-five'))
          ->toString(),
      Link::fromTextAndUrl('Finished', Url::fromUri('internal:' . '/multistep_form/multistep-six'))
          ->toString(),
    );
    return $steps;
  }

}
